### setup environment
module purge
module switch dfldatadir/ra4151
module load ccc/1.0 flavor/buildtarget/x86_64 feature/mkl/single_node flavor/buildmpi/openmpi/4.0 flavor/openmpi/standard feature/openmpi/net/auto .tuning/openmpi/4.0.2 hwloc/2.0.4 sharp/2.0 hcoll/4.4.2938 pmix/3.1.3 ucx/1.7.0 flavor/buildcompiler/intel/19 feature/mkl/lp64 feature/mkl/sequential mkl/19.0.5.281 licsrv/intel c/intel/19.0.5.281 c++/intel/19.0.5.281 fortran/intel/19.0.5.281 gnu/8.3.0 intel/19.0.5.281 mpi/openmpi/4.0.2 datadir/ra4151 dfldatadir/ra4151 python3/3.6.4 git/2.19.1 cmake/3.9.1 flavor/boost/standard boost/1.69.0 eigen/3.2.8 hdf5/1.8.20 gsl/2.1 root/6.18.04 yaml-cpp/0.6.2
export LIBRARY_PATH="$LIBRARY_PATH:/ccc/products/ifort-19.0.5.281/system/default/19.0.5.281/lib/intel64"
export GSL_ROOT_DIR="/ccc/products/gsl-2.1/intel--19.0.5.281/default"
export PKG_CONFIG_PATH=${GSL_ROOT_DIR}/lib/pkgconfig/:${PKG_CONFIG_PATH}

### move to WORK dir
cd $CCCWORKDIR

### clone gambit
GAMBIT_DIR_NAME=gambit_THDM
git clone -b THDM_stable https://phab.hepforge.org/source/gambitgit.git ${GAMBIT_DIR_NAME}
gambit_dir=`pwd`/${GAMBIT_DIR_NAME}

### begin cmake (note this is the debug for testing the build, use the release when you are planning to run scans)
cd $gambit_dir && mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_ROOT=ON -DWITH_MPI=ON -DBUILD_FS_MODELS="THDM_I;THDM_II;THDM_LS;THDM_flipped" -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DWITH_AXEL=ON -DEIGEN3_INCLUDE_DIR=/ccc/products/eigen-3.2.8/default/include/ ..

### make diver (you could try making scanners here if you're brave)
make diver

### set useful variables to make backends
backends_dir=${gambit_dir}/Backends

### start making backends

### make THDMC
name=THDMC
ver=1.8.0
install_dir=${backends_dir}/installed/${name}/${ver}
# make
make ${name}
# do BOSS bug patching
patch_dir=${backends_dir}/patches/${name}/${ver}
patch -u ${install_dir}/src/BOSS_SM.cpp -i ${patch_dir}/BOSS_SM.cpp.patch
patch -u ${install_dir}/src/BOSS_THDM.cpp -i ${patch_dir}/BOSS_THDM.cpp.patch
# make again
make ${name}

# --- higgsbounds 5.8.0 ---
name=higgsbounds
make ${name}

# --- higgssignals 2.5.0 ---
name=higgssignals
make ${name}

# --- higgssignals 2.5.0 ---
name=higgssignals
make ${name}

# --- superiso 4.1 ----
name=superiso
make ${name}

# note this assumes gcc-8.3.0 (which is the needed version for root anyway)
# --- heplike data 1.0 ----
name=heplikedata
make ${name}
# --- heplike 1.0 ----
name=heplike
ver=1.0
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
make ${name}
cd ${install_dir}
# could just export CMAKE_CXX_FLAGS and skip this step?
cmake -DCMAKE_CXX_FLAGS="-I/ccc/products/boost-1.69.0/gcc--8.3.0__openmpi--4.0.1/default/include/ -I/ccc/products/yaml-cpp-0.6.2/system/default/include" .
cd ${gambit_dir}/build
make ${name}

### final cmake gambit
cd ${gambit_dir}/build
cmake …

### get yaml files and scripts
cd ${gambit_dir}/yaml_files
git clone https://gitlab.com/filko/thdm_jc_jobs.git THDM_JC
