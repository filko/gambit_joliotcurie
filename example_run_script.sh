#!/bin/bash 
#MSUB -r gamb_THDM              # Request name 
#MSUB -n 64                     # Total number of tasks to use 
#MSUB -c 16                     # Number of threads per task to use 
#MSUB -T 86400                  # Elapsed time limit in seconds (24 hours max)
#MSUB -o stdo_%I.o              # Standard output. %I is the job id 
#MSUB -e stde_%I.e              # Error output. %I is the job id
#MSUB -A ra4151                 # Project space
#MSUB -q rome                   # Partition
#MSUB -m scratch,work           # file systems

set -x 
cd ${BRIDGE_MSUB_PWD}
export OMP_NUM_THREADS=16
gambit_dir=${BRIDGE_MSUB_PWD}/gambit_THDM
yaml_file=${gambit_dir}/yaml_files/THDM.yaml
# run gambit
ccc_mprun ${gambit_dir}/gambit -rf ${yaml_file}
