### init.sh must be sourced before this is run
# --- superiso 4.1 ----
name=superiso
ver=4.1
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
cd ${gambit_dir}/build
# start make & fudge dl stamp
timeout 2 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
#download backends (in case its been deleted)
/bin/cp -rf /ccc/work/cont005/ra4151/ra4151/backends_downloaded/superiso_v4.1_flavbit.tgz ${gambit_dir}/Backends/downloaded/superiso_v4.1_flavbit.tgz
tar xvzf ${gambit_dir}/Backends/downloaded/${name}_v${ver}_flavbit.tgz
mv ${name}_v${ver}_flavbit/* ${install_dir}
# make
make ${name}
# --------------------
