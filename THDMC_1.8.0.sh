### init.sh must be sourced before this is run
## note this requires castXML to be present
# --- THDMC 1.8.0 ----
name=THDMC
ver=1.8.0
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
cd ${gambit_dir}/build
# start make & fudge dl stamp
timeout 2 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
# download, extract and copy over
curl -x ${proxy} -L https://2hdmc.hepforge.org/downloads/2HDMC-${ver}.tar.gz > ${backends_dir}/downloaded/2HDMC-${ver}.tar.gz
tar xvzf ${backends_dir}/downloaded/2HDMC-${ver}.tar.gz
mv 2HDMC-${ver}/* ${install_dir}
rm -rf 2HDMC-${ver}
# make
make ${name}
# do BOSS bug patching
patch_dir=${backends_dir}/patches/${name}/${ver}
patch -u ${install_dir}/src/BOSS_SM.cpp -i ${patch_dir}/BOSS_SM.cpp.patch
patch -u ${install_dir}/src/BOSS_THDM.cpp -i ${patch_dir}/BOSS_THDM.cpp.patch
# make
make ${name}
# --------------------
