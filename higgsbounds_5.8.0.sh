### init.sh must be sourced before this is run
# --- higgsbounds 5.8.0 ---
name=higgsbounds
ver=5.8.0
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
cd ${gambit_dir}/build
# start make & fudge dl stamp
timeout 2 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
# clone HB
rm -rf ${install_dir}/*
git -c http.proxy=${proxy} clone https://gitlab.com/higgsbounds/higgsbounds.git ${install_dir}
mkdir ${install_dir}/build
# download, extract and copy over LEP tables
filename=lep-chisq-master.tar.gz
curl -x ${proxy} -L https://gitlab.com/higgsbounds/lep-chisq/-/archive/master/${filename} > ${filename}
tar xvzf ${filename}
mkdir -p ${install_dir}/data/lep-chisq-master/csboutput_trans_binary/
mv lep-chisq-master/csboutput_trans_binary/* ${install_dir}/data/lep-chisq-master/csboutput_trans_binary
rm -rf lep-chisq-master
# make
make ${name}
# --------------------
