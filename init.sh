#!/bin/bash

if [ $1 == '-h' ]
then
    echo 'Exports required variables & creates proxy to banach'
    echo 'Usage: source init.h <full_gambit_directory> <port> <banach_username>'
else
    # required vars to set
    export gambit_dir=$1
    export port=$2
    export banach_username=$3
    # more required vars
    export proxy=socks5h://localhost:${port}
    export backends_dir=${gambit_dir}/Backends

    # creates proxy
    ssh -N -f -D ${port} ${banach_username}@banach.cloud.ifj.edu.pl
fi
