### init.sh must be sourced before this is run
# --- diver ---
name=diver
ver=1.0.4
name_ver="${name}_${ver}"
cd ${gambit_dir}/build
# start make & fudge dl stamp
timeout 2 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
# clean up if necessary
rm -rf ${gambit_dir}/ScannerBit/installed/${name}/*
# copy over diver
cp -R /ccc/work/cont005/ra4151/ra4151/scanners_installed/${name}/* ${gambit_dir}/ScannerBit/installed/${name}
# make
make ${name}
# --------------
