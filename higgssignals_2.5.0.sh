# --- higgssignals 2.5.0 ---
name=higgssignals
ver=2.5.0
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
cd ${gambit_dir}/build
# start make & fudge dl stamp
timeout 10 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
# clone HS
rm -rf ${install_dir}/*
git -c http.proxy=${proxy} clone https://gitlab.com/higgsbounds/higgssignals.git ${install_dir}
mkdir ${install_dir}/build
# make
make ${name}
# --------------------
