### init.sh must be sourced before this is run
# --- castXML (linux - within GAMBIT) ---
cd ${gambit_dir}/build
mkdir -p ${backends_dir}/scripts/BOSS/castxml/
curl -x ${proxy} -L https://data.kitware.com/api/v1/file/57b5dea08d777f10f2696379/download > castxml-linuz.tar.gz
tar xvzf castxml-linuz.tar.gz
mv castxml/* ${backends_dir}/scripts/BOSS/castxml
rm -rf castxml/
# --------------------
