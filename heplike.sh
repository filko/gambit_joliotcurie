### init.sh must be sourced before this is run
# note this assumes gcc-8.3.0 (which is the needed version for root anyway)
# --- heplike data 1.0 ----
name=heplikedata
ver=1.0
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
cd ${gambit_dir}/build
# start make & fudge dl stamp
timeout 2 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
# clone HS
rm -rf ${install_dir}
git -c http.proxy=${proxy} clone https://github.com/mchrzasz/HEPLikeData.git ${install_dir}
# make
make ${name}
# --------------------
# --- heplike 1.0 ----
name=heplike
ver=1.0
name_ver="${name}_${ver}"
install_dir=${backends_dir}/installed/${name}/${ver}
# start make & fudge dl stamp
timeout 2 make ${name}
touch ${name_ver}-prefix/src/${name_ver}-stamp/${name_ver}-download
# clone HS
rm -rf ${install_dir}
git -c http.proxy=${proxy} clone https://github.com/mchrzasz/HEPLike.git ${install_dir}
# make (add required includes seperately to cmake)
make ${name}
cd ${install_dir}
# could just export CMAKE_CXX_FLAGS and skip this step?
cmake -DCMAKE_CXX_FLAGS="-I/ccc/products/boost-1.69.0/gcc--8.3.0__openmpi--4.0.1/default/include/ -I/ccc/products/yaml-cpp-0.6.2/system/default/include" .
cd ${gambit_dir}/build
make ${name}
# --------------------
