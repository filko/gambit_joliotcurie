#!/bin/bash 
#MSUB -r make_GAMB              # Request name 
#MSUB -n 12                     # Total number of tasks to use 
#MSUB -c 1                      # Number of threads per task to use 
#MSUB -T 86400                  # Elapsed time limit in seconds (24 hours max)
#MSUB -o stdo_%I.o              # Standard output. %I is the job id 
#MSUB -e stde_%I.e              # Error output. %I is the job id
#MSUB -A ra4151                 # Project space
#MSUB -q rome                   # Partition
#MSUB -m work           # file systems

set -x 
export OMP_NUM_THREADS=1
gambit_dir=${BRIDGE_MSUB_PWD}/gambit_THDM
# make gambit
cd ${gambit_dir}/build
make -j 12
